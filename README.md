# Interview Task
Details can be found in `WorkdayCalculator.pdf`

### Approach taken
- TDD (mostly, not a 100% purist)
- Code as documentation (clean & descriptive as possible)
- I used a HashSet to contain the Holidays because of the O(1) lookup time and because it appears the HashCode of a DateTime object is based on the date itself

### Notes
- This was my first experience with C#, can conclude it's a lovely language to work with and has completely changed my mind on the .NET ecosystem
- Total process took ~6 hours mainly due to a lot of documentation reading, the TDD workflow, dusting off some Dev brain cells & some fun bugs at the end.
- Created a CLI console app for easy testing
- Due to the recursive nature of `calculateNextWorkdayDay` stackoverflows do appear on increments > ~1500 days. After finding this, I could have switched to a more stack-friendly iterative way of calculating but recursion felt cleaner in this instance.
- Overall, had fun!