﻿using WorkdayNet;

class Program
{
    static void Main(string[] args)
    {
        IWorkdayCalendar calendar = new WorkdayCalendar();

        ReadWorkdayStartAndStop();
        SetRecurringHoliday();
        SetHolidays();
        ReadAndReturnWorkdayIncrements();

        void ReadWorkdayStartAndStop()
        {
            int startHour = 8, startMinute = 0, endHour = 16, endMinute = 0;
            Console.WriteLine("Enter the start time of the workday (HH:mm):");
            string? startInput = Console.ReadLine();
            if (string.IsNullOrEmpty(startInput))
            {
                Console.WriteLine("Start input is null or empty, using default start time of 08:00");
            }
            else
            {
                string[] startParts = startInput.Split(':');
                if (startParts.Length != 2)
                {
                    Console.WriteLine("Start input is not in the correct format, using default start time of 08:00");
                }
                else
                {
                    startHour = int.Parse(startParts[0]);
                    startMinute = int.Parse(startParts[1]);
                }
            }

            Console.WriteLine("Enter the end time of the workday (HH:mm):");
            string? endInput = Console.ReadLine();
            if (string.IsNullOrEmpty(endInput))
            {
                Console.WriteLine("End input is null or empty, using default end time of 16:00");
            }
            else
            {
                string[] endParts = endInput.Split(':');
                if (endParts.Length != 2)
                {
                    Console.WriteLine("Start input is not in the correct format, using default start time of 08:00");
                }
                else
                {
                    endHour = int.Parse(endParts[0]);
                    endMinute = int.Parse(endParts[1]);
                }
            }

            Console.WriteLine($"{Environment.NewLine}Setting -- Start: {startHour}:{startMinute}, End: {endHour}:{endMinute}{Environment.NewLine}");
            calendar.SetWorkdayStartAndStop(startHour, startMinute, endHour, endMinute);
            Console.WriteLine("______________________________________________________");
        }

        void SetRecurringHoliday()
        {
            do
            {
                Console.WriteLine("Enter a recurring holiday (MM-dd), one per line:");
                string? rHolidayInput = Console.ReadLine();
                if (string.IsNullOrEmpty(rHolidayInput))
                {
                    Console.WriteLine("Input is empty, ignoring recurring holidays...");
                    Console.WriteLine("______________________________________________________");
                    break;
                };
                string[] parts = rHolidayInput.Split('-');
                if (parts.Length != 2)
                {
                    Console.WriteLine("Input is not in the correct format, try again...");
                    Console.WriteLine("______________________________________________________");
                    continue;
                }
                int month = int.Parse(parts[0]);
                int day = int.Parse(parts[1]);
                Console.WriteLine($"{Environment.NewLine}Setting Recurring Holiday-- Month: {month}, Day:{day} {Environment.NewLine}");
                calendar.SetRecurringHoliday(month, day);
                Console.WriteLine("______________________________________________________");

                calendar.SetRecurringHoliday(month, day);
            } while (true);
        }

        void SetHolidays()
        {
            do
            {
                Console.WriteLine("Enter a holiday (yyyy-MM-dd), one per line:");
                string? holidayInput = Console.ReadLine();
                if (string.IsNullOrEmpty(holidayInput))
                {
                    Console.WriteLine("Input is empty, ignoring recurring holidays...");
                    Console.WriteLine("______________________________________________________");
                    break;
                };
                string[] holidayParts = holidayInput.Split('-');
                if (holidayParts.Length != 3)
                {
                    Console.WriteLine("Input is not in the correct format, try again...");
                    Console.WriteLine("______________________________________________________");
                    continue;
                }
                int holidayYear = int.Parse(holidayParts[0]);
                int holidayMonth = int.Parse(holidayParts[1]);
                int holidayDay = int.Parse(holidayParts[2]);
                Console.WriteLine($"{Environment.NewLine}Setting Holiday-- {holidayYear}/{holidayMonth}/{holidayDay} {Environment.NewLine}");
                calendar.SetHoliday(new DateTime(holidayYear, holidayMonth, holidayDay));
                Console.WriteLine("______________________________________________________");
            } while (true);
        }

        void ReadAndReturnWorkdayIncrements()
        {
            DateTime inputtedDate = DateTime.Now;
            string format = "dd-MM-yyyy HH:mm";
            do
            {
                Console.WriteLine($"Using current DateTime for increment. {Environment.NewLine}Enter a DateTime you'd like to increment workdays to (yyyy-MM-dd HH:mm), one per line:");
                Console.WriteLine("Enter: Y/y to use last date entered");
                string? dateIncrementInput = Console.ReadLine();
                if (string.IsNullOrEmpty(dateIncrementInput))
                {
                    Console.WriteLine("Input is empty, exiting program...");
                    Console.WriteLine("______________________________________________________");
                    break;
                }
                if (dateIncrementInput.Equals("Y", StringComparison.OrdinalIgnoreCase))
                {
                    Console.WriteLine("Using last date entered...");
                    Console.WriteLine("______________________________________________________");
                }
                else
                {
                    string[] dateTimeIncrementParts = dateIncrementInput.Split(' ');
                    if (dateTimeIncrementParts.Length != 2)
                    {
                        Console.WriteLine("DateTime is not in the correct format, try again...");
                        Console.WriteLine("______________________________________________________");
                        continue;
                    }
                    string[] dateParts = dateTimeIncrementParts[0].Split('-');
                    if (dateParts.Length != 3)
                    {
                        Console.WriteLine("Date is not in the correct format, try again...");
                        Console.WriteLine("______________________________________________________");
                        continue;
                    }
                    string[] timeParts = dateTimeIncrementParts[1].Split(':');
                    if (timeParts.Length != 2)
                    {
                        Console.WriteLine("Time is not in the correct format, try again...");
                        Console.WriteLine("______________________________________________________");
                        continue;
                    }
                    int year = int.Parse(dateParts[0]);
                    int month = int.Parse(dateParts[1]);
                    int day = int.Parse(dateParts[2]);
                    int hour = int.Parse(timeParts[0]);
                    int minute = int.Parse(timeParts[1]);
                    inputtedDate = new DateTime(year, month, day, hour, minute, 0);
                }
                
                Console.WriteLine($"Workday inputted: {inputtedDate.ToString(format)} {Environment.NewLine}");

                Console.WriteLine("Enter the number of workdays to increment or decrement by (decimal input - ie: -1.37 or 8.2321):");
                string? incrementInput = Console.ReadLine();
                if (string.IsNullOrEmpty(incrementInput))
                {
                    Console.WriteLine("Input is empty, try again...");
                    Console.WriteLine("______________________________________________________");
                    continue;
                }
                decimal increment = Decimal.Parse(incrementInput);
                var incrementedDate = calendar.GetWorkdayIncrement(inputtedDate, increment);
                Console.WriteLine($"{Environment.NewLine}{inputtedDate.ToString(format)} with an addition of {increment} work days is {incrementedDate.ToString(format)}{Environment.NewLine}");
                Console.WriteLine("______________________________________________________");
            } while (true);
            return;
        }
    }
}