namespace WorkdayNet;
using System.Collections.Generic;

public class WorkdayCalendar : IWorkdayCalendar
{
    private const int RECURRING_YEAR_PLACEHOLDER = 1004;
    private TimeSpan _workdayLength;
    private TimeSpan _workdayBeginning;
    private TimeSpan _workdayEnd;
    private HashSet<DateTime> _holidayRecords = new HashSet<DateTime>();
    private HashSet<DateTime> _recurringHolidayRecords = new HashSet<DateTime>();

    public DateTime GetWorkdayIncrement(DateTime startDate, decimal incrementInWorkdays)
    {
        (int dateIncrement, decimal timeIncrement) = splitDayTimeIncrement(incrementInWorkdays);

        DateTime newIncrementedWorkday = adjustStartDateToWorkday(startDate, timeIncrement);

        newIncrementedWorkday = incrementTimeInWorkday(newIncrementedWorkday, timeIncrement);

        return calculateNextWorkdayDay(dateIncrement, newIncrementedWorkday);
    }

    public void SetHoliday(DateTime date)
    {
        this._holidayRecords.Add(date);
    }

    public void SetRecurringHoliday(int month, int day)
    {
        this._recurringHolidayRecords.Add(new DateTime(RECURRING_YEAR_PLACEHOLDER, month, day));
    }

    public void SetWorkdayStartAndStop(int startHours, int startMinutes, int stopHours, int stopMinutes)
    {
        _workdayBeginning = new TimeSpan(startHours, startMinutes, 0);
        _workdayEnd = new TimeSpan(stopHours, stopMinutes, 0);
        _workdayLength = _workdayEnd - _workdayBeginning;
    }

    private DateTime adjustStartDateToWorkday(DateTime startDate, decimal increment)
    {
        TimeSpan startingTime = startDate.TimeOfDay;
        if (startingTime > _workdayBeginning && startingTime < _workdayEnd) return startDate;

        bool isDecrementing = increment < 0;
        bool isIncrementing = increment > 0 || increment == 0; // assuming 0 is incrementing
        bool isStartBeforeWorkday = startingTime < _workdayBeginning;
        bool isStartAfterWorkday = startingTime > _workdayEnd;

        if (isIncrementing && isStartBeforeWorkday)
        {
            return startDate.Date.Add(_workdayBeginning);
        }

        if (isDecrementing && isStartBeforeWorkday)
        {
            return startDate.Date.AddDays(-1).Add(_workdayEnd);
        }

        if (isIncrementing && isStartAfterWorkday)
        {
            return startDate.Date.AddDays(1).Add(_workdayBeginning);
        }

        if (isDecrementing && isStartAfterWorkday)
        {
            return startDate.Date.Add(_workdayEnd);
        }
        return startDate;
    }

    private DateTime calculateNextWorkdayDay(int dateIncrement, DateTime newIncrementedWorkday)
    {
        if (dateIncrement == 0)
        {
            return newIncrementedWorkday;
        }
        int nextDayIncrementDirection = dateIncrement > 0 ? 1 : -1;
        newIncrementedWorkday = newIncrementedWorkday.AddDays(nextDayIncrementDirection);

        bool isWeekend = newIncrementedWorkday.DayOfWeek == DayOfWeek.Saturday || newIncrementedWorkday.DayOfWeek == DayOfWeek.Sunday;
        if (isWeekend)
        {
            return calculateNextWorkdayDay(dateIncrement, newIncrementedWorkday);
        }

        bool isRecurringHoliday = this._recurringHolidayRecords.Contains(new DateTime(RECURRING_YEAR_PLACEHOLDER, newIncrementedWorkday.Month, newIncrementedWorkday.Day));
        bool isHoliday = this._holidayRecords.Contains(newIncrementedWorkday.Date);
        if (isRecurringHoliday || isHoliday)
        {
            return calculateNextWorkdayDay(dateIncrement, newIncrementedWorkday);
        }

        return calculateNextWorkdayDay(dateIncrement - nextDayIncrementDirection, newIncrementedWorkday);
    }

    private DateTime incrementTimeInWorkday(DateTime newIncrementedWorkday, decimal timeIncrement)
    {
        return newIncrementedWorkday.Add(_workdayLength * (double)timeIncrement);
    }

    private (int dateIncrement, decimal timeIncrement) splitDayTimeIncrement(decimal incrementInWorkdays)
    {
        return (dateIncrement: (int)incrementInWorkdays, timeIncrement: incrementInWorkdays % 1);
    }
}