using FluentAssertions;
using WorkdayNet;

namespace WorkdayCalendar.Tests;

public class WorkdayCalendarTests
{
    const int TestYear = 2023;
    const int TestMonth = 3;
    const int DefaultTestDay = 15;
    DateTime DefaultTestDate;
    IWorkdayCalendar sut;

    public WorkdayCalendarTests()
    {
        DefaultTestDate = new DateTime(TestYear, TestMonth, DefaultTestDay);
        sut = new WorkdayNet.WorkdayCalendar();
    }

    /* INCREMENTING & DECREMENTING WORKDAY TESTS */

    [Fact]
    public void ShouldNotConsiderWeekendsAsWorkdays()
    {
        int[] twoWeeks = Enumerable.Range(-7, 7).ToArray();

        foreach (int dayIncrement in twoWeeks)
        {
            DateTime result = sut.GetWorkdayIncrement(DefaultTestDate, dayIncrement);

            result.DayOfWeek.Should().NotBe(DayOfWeek.Saturday);
            result.DayOfWeek.Should().NotBe(DayOfWeek.Sunday);
        }
    }

    [Theory]
    [InlineData(15, 2, 17)]
    [InlineData(15, -2, 13)]
    [InlineData(15, 10, 29)]
    [InlineData(1, 20, 29)]
    [InlineData(30, -21, 1)]
    public void ShouldIncrementAndDecrementWorkDaysInTheCalendar(int testDay, int workdayIncrement, int expectedDay)
    {
        DateTime testDate = new DateTime(TestYear, TestMonth, testDay);

        DateTime result = sut.GetWorkdayIncrement(testDate, workdayIncrement);

        result.Day.Should().Be(expectedDay);
    }

    public static TheoryData<decimal, DateTime> DecimalIncrementData =>
        new TheoryData<decimal, DateTime>
            {
                { 0.75m, new DateTime(2023, 03, 15, 14, 0, 0) },
                { 1.5m, new DateTime(2023, 03, 16, 12, 0, 0) },
                { -1.5m, new DateTime(2023, 03, 13, 12, 0, 0) },
                { -1.3273m, new DateTime(2023, 03, 13, 13, 22, 53, 760) },
                { 4.123456m, new DateTime(2023, 03, 21, 8, 59, 15, 532, 800) },
                { 5.96m, new DateTime(2023, 03, 22, 15, 40, 48) },
            };

    [Theory]
    [MemberData(nameof(DecimalIncrementData))]
    public void ShouldReturnADateTimeEquivalentToDecimalIncrement(decimal testIncrement, DateTime expectedDateTime)
    {
        sut.SetWorkdayStartAndStop(8, 0, 16, 0);

        DateTime result = sut.GetWorkdayIncrement(DefaultTestDate, testIncrement);

        result.Should().Be(expectedDateTime);
    }

    [Fact]
    public void ShouldReturnAFridayWhenDecrementingFromAMonday()
    {
        DateTime testDate = new DateTime(TestYear, TestMonth, 13);

        DateTime result = sut.GetWorkdayIncrement(testDate, -1);

        result.DayOfWeek.Should().Be(DayOfWeek.Friday);
        result.Day.Should().Be(10);
    }

    /* SETTING HOLIDAY TESTS */

    [Fact]
    public void ShouldSetHolidayThatWillBeNotTakenAsWorkdayWhenAskedForAnIncrement()
    {
        sut.SetHoliday(new DateTime(TestYear, TestMonth, 16));

        DateTime result = sut.GetWorkdayIncrement(DefaultTestDate, 1);

        result.Day.Should().Be(17);
    }

    [Fact]
    public void ShouldIgnoreMultipleConsequentHolidaysBackToBackWhenAskedForAnIncrement()
    {
        var startWeekDay = 13;
        var startOfNextWeekDay = 20;
        sut.SetHoliday(new DateTime(TestYear, TestMonth, startWeekDay));
        sut.SetHoliday(new DateTime(TestYear, TestMonth, startWeekDay + 1));
        sut.SetHoliday(new DateTime(TestYear, TestMonth, startWeekDay + 2));
        sut.SetHoliday(new DateTime(TestYear, TestMonth, startWeekDay + 3));
        sut.SetHoliday(new DateTime(TestYear, TestMonth, startWeekDay + 4));

        DateTime startOfWeekDate = new DateTime(TestYear, TestMonth, startWeekDay);

        DateTime result = sut.GetWorkdayIncrement(DefaultTestDate, 1);

        result.Day.Should().Be(startOfNextWeekDay);
    }

    /* SETTING REOCCURING HOLIDAY TESTS */

    [Fact]
    public void ShouldSetAHolidayThatAppliesEveryYear()
    {
        var dateInMarchWithThreeConsecutiveWorkdays = 13;
        var testRecurringHolidayDate = 14;
        DateTime testDateYearOne = new DateTime(TestYear, TestMonth, dateInMarchWithThreeConsecutiveWorkdays);
        DateTime testDateYearTwo = new DateTime(TestYear + 1, TestMonth, dateInMarchWithThreeConsecutiveWorkdays);
        DateTime testDateYearThree = new DateTime(TestYear + 2, TestMonth, dateInMarchWithThreeConsecutiveWorkdays);

        sut.SetRecurringHoliday(TestMonth, testRecurringHolidayDate);

        DateTime resultYearOne = sut.GetWorkdayIncrement(testDateYearOne, 1);
        DateTime resultYearTwo = sut.GetWorkdayIncrement(testDateYearTwo, 1);
        DateTime resultYearThree = sut.GetWorkdayIncrement(testDateYearThree, 1);

        resultYearOne.Day.Should().Be(testRecurringHolidayDate + 1);
        resultYearOne.DayOfWeek.Should().Be(DayOfWeek.Wednesday);

        resultYearTwo.Day.Should().Be(testRecurringHolidayDate + 1);
        resultYearTwo.DayOfWeek.Should().Be(DayOfWeek.Friday);

        resultYearThree.Day.Should().Be(17);
        resultYearThree.DayOfWeek.Should().Be(DayOfWeek.Monday);
    }

    /* SETTING WORK HOURS TESTS */

    [Fact]
    public void ShouldChangeWorkingHoursWhenWorkdayStartAndStopSet()
    {
        sut.SetWorkdayStartAndStop(10, 30, 18, 30);

        DateTime result1 = sut.GetWorkdayIncrement(DefaultTestDate, 1.5m);

        result1.Should().Be(new DateTime(TestYear, TestMonth, DefaultTestDay + 1, 14, 30, 0));


        sut.SetWorkdayStartAndStop(8, 15, 18, 15);

        DateTime result2 = sut.GetWorkdayIncrement(DefaultTestDate, 1.25m);

        result2.Should().Be(new DateTime(TestYear, TestMonth, DefaultTestDay + 1, 10, 45, 0));
    }

    [Theory]
    [InlineData(10, 30, 18, 30, 14, 30)]
    [InlineData(8, 15, 18, 15, 13, 15)]
    [InlineData(10, 30, 11, 00, 10, 45)]
    [InlineData(10, 11, 10, 13, 10, 12)]
    public void ShouldAdjustWorkdayHours(int startHours, int startMinutes, int stopHours, int stopMinutes, int expectedHour, int expectedMinutes)
    {
        sut.SetWorkdayStartAndStop(startHours, startMinutes, stopHours, stopMinutes);

        DateTime result = sut.GetWorkdayIncrement(DefaultTestDate, 0.5m);

        result.TimeOfDay.Should().Be(new TimeSpan(expectedHour, expectedMinutes, 0));
    }

    /* OUT OF HOURS */

    [Fact]
    public void ShouldAdvanceABeforeHoursDateTimeToNextWorkingDayHours_EvenWhenZeroIncrementAsked()
    {
        DateTime testDateTime = new DateTime(TestYear, TestMonth, DefaultTestDay, 2, 17, 12);
        const int StartHours = 8;
        sut.SetWorkdayStartAndStop(StartHours, 0, 16, 0);

        DateTime result = sut.GetWorkdayIncrement(testDateTime, 0);

        result.Date.Should().Be(testDateTime.Date);
        result.TimeOfDay.Should().Be(new TimeSpan(StartHours, 0, 0));
    }

    [Fact]
    public void ShouldAdvanceAnAfterHoursDateTimeToNextWorkingDayHours_EvenWhenZeroIncrementAsked()
    {
        DateTime testDateTime = new DateTime(TestYear, TestMonth, DefaultTestDay, 22, 17, 12);
        const int StartHours = 8;
        sut.SetWorkdayStartAndStop(StartHours, 0, 16, 0);

        DateTime result = sut.GetWorkdayIncrement(testDateTime, 0);

        result.Date.Should().Be(testDateTime.Date.AddDays(1));
        result.TimeOfDay.Should().Be(new TimeSpan(StartHours, 0, 0));
    }

    [Theory]
    [InlineData(0, 0, 0.25, 10)]
    [InlineData(4, 30, 0.5, 12)]
    [InlineData(2, 13, 0.75, 14)]
    public void ShouldReturnWorkdayInBusinessHoursEvenWhenStartDateIsBeforeBusinessHours(int startHours, int startMinutes, decimal increment, int exepectedHours)
    {
        DateTime testDateTime = new DateTime(TestYear, TestMonth, DefaultTestDay, startHours, startMinutes, 0);

        sut.SetWorkdayStartAndStop(8, 0, 16, 0);

        DateTime result = sut.GetWorkdayIncrement(testDateTime, increment);

        result.Date.Should().Be(testDateTime.Date);
        result.TimeOfDay.Should().Be(new TimeSpan(exepectedHours, 0, 0));
    }

    [Fact]
    public void ShouldReturnToPreviousWorkdayWhenDecrementingAndHoursAreBeforeBusinessHours()
    {
        DateTime testDateTime = new DateTime(TestYear, TestMonth, DefaultTestDay, 4, 0, 0);

        sut.SetWorkdayStartAndStop(8, 0, 16, 0);

        DateTime result = sut.GetWorkdayIncrement(testDateTime, -0.25m);

        result.Date.Should().Be(testDateTime.Date.AddDays(-1));
        result.TimeOfDay.Should().Be(new TimeSpan(14, 0, 0));
    }

    [Theory]
    [InlineData(19, 28, 0.25, 10)]
    [InlineData(22, 19, 0.5, 12)]
    [InlineData(19, 28, 0.75, 14)]
    public void ShouldAdvanceWorkDayToNextDayIfHoursGivenAreAfterWorkingDayEnd(int startHours, int startMinutes, decimal increment, int exepectedHours)
    {
        DateTime testDateTime = new DateTime(TestYear, TestMonth, DefaultTestDay, startHours, startMinutes, 0);

        sut.SetWorkdayStartAndStop(8, 0, 16, 0);

        DateTime result = sut.GetWorkdayIncrement(testDateTime, increment);

        result.Date.Should().Be(testDateTime.Date.AddDays(1));
        result.TimeOfDay.Should().Be(new TimeSpan(exepectedHours, 0, 0));
    }

    [Fact]
    public void ShouldReturnToWorkdayWhenDecrementingAndHoursAreAfterWorkingDayEnd()
    {
        DateTime testDateTime = new DateTime(TestYear, TestMonth, DefaultTestDay, 20, 0, 0);

        sut.SetWorkdayStartAndStop(8, 0, 16, 0);

        DateTime result = sut.GetWorkdayIncrement(testDateTime, -0.5m);

        result.Date.Should().Be(testDateTime.Date);
        result.TimeOfDay.Should().Be(new TimeSpan(12, 0, 0));
    }

    [Theory]
    [InlineData(-1094.53453)]
    [InlineData(-1.375)]
    [InlineData(0)]
    [InlineData(0.5283)]
    [InlineData(1455.523)]
    public void ShouldNotReturnAWorkdayOutsideOfSetWorkingHours(double increment)
    {
        const int StartHours = 10;
        const int StartMinutes = 30;
        const int StopHours = 18;
        const int StopMinutes = 30;
        sut.SetWorkdayStartAndStop(StartHours, StartMinutes, StopHours, StopMinutes);

        DateTime result = sut.GetWorkdayIncrement(DefaultTestDate, (decimal)increment);

        result.TimeOfDay.Should().BeGreaterOrEqualTo(new TimeSpan(StartHours, StartMinutes, 0));
        result.TimeOfDay.Should().BeLessOrEqualTo(new TimeSpan(StopHours, StopMinutes, 0));
    }

    /* E2E */

    [Fact]
    public void E2E_ShouldWorkAllTogetherAsExpected()
    {
        sut.SetWorkdayStartAndStop(8, 0, 16, 0);
        sut.SetRecurringHoliday(5, 17);
        sut.SetHoliday(new DateTime(2004, 5, 27));

        var incrementedDate = sut.GetWorkdayIncrement(new DateTime(2004, 5, 24, 18, 5, 0), -5.5m);

        incrementedDate.Should().Be(new DateTime(2004, 5, 14, 12, 0, 0));
    }
}


